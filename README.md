# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.2/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.2/maven-plugin/reference/html/#build-image)
* [Jersey](https://docs.spring.io/spring-boot/docs/2.5.2/reference/htmlsingle/#boot-features-jersey)
* [Swagger](https://github.com/swagger-api/swagger-ui)

### Service REST
Use CLI to consume the rest service

#### Test
```
mvn test
```

#### Api PI
```
curl -X GET "http://localhost:8080/api/v1/challenge/pi?random_limit=10" -H "Content-Type: application/json"
```

#### the Swagger UI
```
open http://localhost:8080/swagger/index.html
```
