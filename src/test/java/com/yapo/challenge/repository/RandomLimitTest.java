package com.yapo.challenge.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class RandomLimitTest {

    @Autowired
    RandomLimit randomLimit;

    @BeforeEach
    void setUp() {
        randomLimit = new RandomLimit( 10 );
        randomLimit.formatNumberDecimal( Math.PI );
    }

    @Test
    void getAverage() {
        Assertions.assertEquals(5, randomLimit.getAverage());
    }

    @Test
    void getRange() {
        Assertions.assertEquals( 6 , randomLimit.getRange());
    }

    @Test
    void getLimit() {
        if (randomLimit.getLimit() >= 5 && 10 >= randomLimit.getLimit()) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertTrue( false);
        }
    }

    @Test
    void getFormat() {
        switch(randomLimit.getLimit()) {
            case 5:  Assertions.assertEquals("3.14159", randomLimit.getFormat()); break;
            case 6:  Assertions.assertEquals("3.141593", randomLimit.getFormat()); break;
            case 7:  Assertions.assertEquals("3.1415927", randomLimit.getFormat()); break;
            case 8:  Assertions.assertEquals("3.14159265", randomLimit.getFormat()); break;
            case 9:  Assertions.assertEquals("3.141592654", randomLimit.getFormat()); break;
            case 10: Assertions.assertEquals("3.1415926536", randomLimit.getFormat()); break;
            default: break;
        }
    }

}
