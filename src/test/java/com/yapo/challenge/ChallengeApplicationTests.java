package com.yapo.challenge;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ChallengeApplicationTests {

	@Test
	void main() {
		ChallengeApplication.main(new String[] {});
	}

}
