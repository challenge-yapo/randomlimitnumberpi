package com.yapo.challenge.model;

import java.io.Serializable;

public class Challenge implements Serializable {

    private static final long serialVersionUID = -4434868206171224257L;

    private int param;

    public int getParam() {
        return this.param;
    }

    public void setParam(int param) {
        this.param = param;
    }
}
