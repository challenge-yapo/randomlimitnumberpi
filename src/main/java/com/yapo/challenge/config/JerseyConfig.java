package com.yapo.challenge.config;

import com.yapo.challenge.service.ChallengeService;
import javax.annotation.PostConstruct;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

@Component
@XmlRootElement
public class JerseyConfig extends ResourceConfig {

    @Value("${spring.jersey.application-path:/api}")
    private String apiPath;

    public JerseyConfig() {
        this.registerEndpoints();
    }

    @PostConstruct
    public void init() {
        this.configureSwagger();
    }

    private void registerEndpoints() {
        this.register(ChallengeService.class);
        this.register(WadlResource.class);
    }

    private void configureSwagger() {
        this.register(ApiListingResource.class);
        this.register(SwaggerSerializers.class);

        BeanConfig config = new BeanConfig();
        config.setTitle("Restful API by Spring Boot, Jersey, Swagger");
        config.setVersion("v1");
        config.setContact("manuel.suarez.molina@gmail.com");
        config.setSchemes(new String[] { "http", "https" });
        config.setBasePath(this.apiPath);
        config.setResourcePackage("com.yapo.challenge.service");
        config.setPrettyPrint(true);
        config.setScan(true);
    }

}
