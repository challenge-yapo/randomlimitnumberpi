package com.yapo.challenge.repository;

import java.text.DecimalFormat;

public class RandomLimit implements IRandomLimit{

    private int randomLimit;
    private int average;
    private int range;
    private int limit;
    private String format;

    public RandomLimit(int randomLimit) {
        this.randomLimit = randomLimit;
        this.average = Math.round(this.randomLimit / DIVISOR);
        this.range = this.randomLimit - this.average +1;
        this.limit = (int) Math.floor(Math.random()*this.range+this.average);
    }

    @Override
    public int getAverage() {
        return this.average;
    }

    @Override
    public int getRange() {
        return this.range;
    }

    @Override
    public int getLimit() {
        return this.limit;
    }

    @Override
    public String getFormat() {
        return this.format;
    }

    @Override
    public void formatNumberDecimal( double number ) {
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits( this.limit );
        this.format = decimalFormat.format(number);
    }
}
