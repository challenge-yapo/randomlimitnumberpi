package com.yapo.challenge.repository;

public class RandomLimitNumberPI extends RandomLimit {

    public RandomLimitNumberPI(int randomLimit) {
        super(randomLimit);
        this.formatNumberDecimal( Math.PI );
    }

    @Override
    public void formatNumberDecimal(double number) {
        super.formatNumberDecimal(number);
    }
}
