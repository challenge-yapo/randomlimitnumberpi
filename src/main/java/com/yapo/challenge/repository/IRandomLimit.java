package com.yapo.challenge.repository;

public interface IRandomLimit {
    float DIVISOR = 2f;

    int getAverage();
    int getRange();
    int getLimit();
    String getFormat();
    void formatNumberDecimal(double number);
}
