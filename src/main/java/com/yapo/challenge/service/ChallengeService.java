package com.yapo.challenge.service;

import com.yapo.challenge.model.Challenge;
import com.yapo.challenge.repository.RandomLimitNumberPI;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/v1/challenge")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Challenge Yapo", produces = "application/json")
public class ChallengeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChallengeService.class);

    @GET                        //JAX-RS Annotation
    @Path("/pi")	            //JAX-RS Annotation
    @ApiOperation(				//Swagger Annotation
            value = "Returns decimals of pi",
            response = Challenge.class)
    @ApiResponses(value = {		//Swagger Annotation
            @ApiResponse(code = 200, message = "Resource found", response = String.class),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    public Response getDecimalPI (
            @ApiParam(value="input param", required = true) @QueryParam("random_limit") int randomLimit) {
        LOGGER.info("/v1/challenge/pi?{}", randomLimit);

        RandomLimitNumberPI randomLimitNumberPI = new RandomLimitNumberPI(randomLimit);
        LOGGER.info("average : {}", randomLimitNumberPI.getAverage());
        LOGGER.info("range : {}", randomLimitNumberPI.getRange());
        LOGGER.info("limit : {}", randomLimitNumberPI.getLimit());
        LOGGER.info("format : {}", randomLimitNumberPI.getFormat());

        final String newPI = randomLimitNumberPI.getFormat();

        return Response.ok(newPI).build();
    }
}
